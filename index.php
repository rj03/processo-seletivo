<!DOCTYPE html>
<html>
<head>
	<title>Processo Seletivo</title>
	<link rel="shortcut icon" href="images/bitmap.png" />
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.o, maximun-scale=1.0">
	<meta charset ="description" content="descrição da pagina (metas tags para SEO)"/>
	<meta charset= "keyword" content="palavras chaves separadas por , (metas tags para SEO)"/>
	<meta charset= "author" content="Raphael de Jesus Bonifácio"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
</head>
<body>
	<header>
		<div class="container">
			<div class="img_logo"></div><!-- img_logo -->
		</div><!-- container -->
	</header>
	<section class="sessao_pesquisa">
		<div class="container">
			<div class="form_pesquisa">
				<form>
					<input type="text" name="input_pesquisa" class="input_pesquisa"/><!-- input_pesquisa -->
					<select class="select_pesquisa">
					      <option value="todas" selected id="todas">Todas</option>
					      <option value="noticias" id="noticias">Noticias</option>
					      <option value="foto" id="foto">Fotos</option>
					</select><!-- select_pesquisa -->
					<input type="submit" name="acao_pesquisa" class="btn_pesquisa" value="Pesquisar"><!-- btn_pesquisa -->
				</form>
				<div class="clear"></div><!-- clear -->
				<div class="aux_pesquisa">
					<p>Ordenar por:</p>
					<select class="select_aux_pesquisa">
					      <option value="recentes" selected>Mais recentes</option>
					      <option value="antigas">Mais antigas</option>
					</select><!-- select_aux_pesquisa -->
				</div><!-- aux_pesquisa -->
			</div><!-- form_pesquisa -->
		</div><!-- container -->
	</section><!-- sessao_pesquisa -->
	<section class="sessao_corpo_pesquisa">
		<div class="container">

			<div class="infor_resultado">
				<div class="barra_filter">
					
				</div><!-- barra_filter -->
				<p></p>
			</div><!-- infor_resultado -->

			<div class="dados_resultado">

			</div><!-- dados_resultado -->
		</div><!-- container -->
	</section><!-- corpo_pesquisa -->

	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/sistemaBusca.js"></script>
</body>
</html>